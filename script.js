function createNewUser() {

    const newUser = {

        _firstName: prompt('Enter First Name'),
        _lastName: prompt('Enter Last Name'),
        birthday: prompt('Enter your birthday dd.mm.yyyy'),

        getLogin() {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
        },

        getAge() {
            
            const birthdayObj = this.birthday.split('.');
            const year = Number(birthdayObj[2]);
            const month = Number(birthdayObj[1]);
            const day = Number(birthdayObj[0]);

            let now = new Date();

            let age = now.getFullYear() - year;
            let m = now.getMonth() + 1 - month;
            let d = now.getDate() - day;

            if (m < 0 || (m === 0 && d < 0)) {
                age--;
            }
            return age;
        },

        getPassword() {
            const birthdayObj = this.birthday.split('.');
            const year = Number(birthdayObj[2]);
            return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + year;
        }
    };

    Object.defineProperty(newUser, "firstName", {
        set: function setFirstName(value) {
            this._firstName = value;
        },
        get: function setFirstName() {
            return this._firstName;
        },
    });

    Object.defineProperty(newUser, "lastName", {
        set: function setLastName(value) {
            this._lastName = value;
        },
        get: function setLastName() {
            return this._lastName;
        },
    });

    return newUser;
    
}

let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getLogin());

console.log(newUser.getAge());
console.log(newUser.getPassword());

